Projet de modélisation d'un jeu de société : le roi des roses dans le cadre de l'UE prog impérative en python, master 1 bioinfo.  
Les règles originales sont quelques peu modifiées et l'usage de classes dans le code est prohibé, le fichier ia faisant exception.  
Executer le fichier roi\_des\_roses.py executera 1 ou n parties de 2 joueurs s'affrontant. Chaque joueur peut etre un joueur humain, randomChoice, algoNegamax, algoAlphaBeta ou algoMonteCarloTreeSearch.  
L'objectif final étant un concours entre IA d'étudiants, le fichier ia.py est construit de telle manière qu'il sert d'instance d'IA, impliquant l'usage de variables globales et interdisant par exemple un match entre 2 IAs, un second fichier ia2.py permet de résoudre le problème.

	
MODELE / Regles:

	init : 
		plateau de 9x9
		52 pions d'influence recto-blancs / verso-rouge
		24 cartes d'influence
			3 serie I II et III
				8 positions d'épée
		
		5 cartes influences chacun tirées au hasard puis visibles
		pion couronne au milieu du plateau
		Rouge commence, puis tour a tour
		
		
		
	jeu : 
		a chaque tour 2 choix de jeu :
		
			pioche :
				pioche une carte d'influence depuis la pioche,
				possible uniquement si le joueur a moins de 5 cartes
				si pioche vide, defausse melangée et remise a dispo
			
			moveCouronne : 
				utilisation d'1 carte influence pour faire un move,
					perte de la carte (defausse) 
				mouvement de I II ou II case dans la direction de l'epee
				le mouvement doit etre entier, sans sortir, sans finir sur une case occupée par un pion 
				sur la case d'arrivée : pion d'influence de la couleur du joueur + pion couronne
			
					
					
	fin : 
		2 cas possibles :
			aucune des 5 cartes des 2 joueurs n'est jouable
			ou
			un joueur pose le dernier pion d'influence de sa couleur (sur les 26)
			
	score : somme des surfaces occupées, 
		1 surface de n pion adjacents (pas en diagonal) = n*n
		
	Celui qui a obtenu le plus grand nombre de points gagne la partie. Egalité sinon.
