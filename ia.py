from roi_des_roses import *
import random
import math
import copy
import time

''' CONSTANTES '''

PROF_MINMAX_ALPHABETA = 5
#vs random, 5 de prof = 300s pour 100 games

SEARCH_TIME_MCTS = 2.95 #en seconde
#vs random, 1s de reflexion = 50s de game
#vs random, 3s de reflexion = 130s de game


''' VARIABLES GLOBALES '''

pioche = initPioche()
defausse, mainR, mainB = [], [], [] #on garde en memoire les mains pour pouvoir suivre l'evolution de la pioche sans relire le plateau
premierCoup = True
nbPionRouge, nbPionBlanc = 26, 26

def reset():
    global pioche, defausse, mainR, mainB, premierCoup, nbPionRouge, nbPionBlanc

    pioche = initPioche()
    defausse, mainR, mainB = [], [], [] #on garde en memoire les mains pour pouvoir suivre l'evolution de la pioche sans relire le plateau
    premierCoup = True
    nbPionRouge, nbPionBlanc = 26, 26


''' PLAYS ET NAME '''

def name():
    return "Le Comte de Monte-Carlo"


def play(plateau, l_roi, c_roi, main_r, main_b, couleur):
    ''' ON ACTUALISE TOUTES LES INFOS DU JEU QUON A EN TANT QUE JOUEUR = LES 2 DERNIERS COUPS '''
    global pioche, defausse, mainR, mainB, premierCoup, nbPionRouge, nbPionBlanc

    if premierCoup:
        premierCoup = False
        observeFirstMove(plateau, l_roi, c_roi, main_r, main_b, couleur)
        
    else:
        observeLast2Moves(main_r, main_b, couleur)

        
    ''' FIN ACTUALISATION / OBSERVATION, APPEL A LA FONCTION DE JEU '''

    #action = playNegamax(plateau, l_roi, c_roi, mainR, mainB, pioche, defausse, couleur, nbPionRouge, nbPionBlanc)
    action = mcts(State(plateau, l_roi, c_roi, mainR, mainB, pioche, defausse, couleur, nbPionRouge, nbPionBlanc))

    return action


def playTataSuzanne(plateau, l_roi, c_roi, main_r, main_b, couleur):
    main = main_b
    if couleur == "Rouge":
        main = main_r
    if len(main)<5:
        return "pioche"
    else:
        for carte in main:
            if mouvement_possible(plateau, l_roi, c_roi, carte):
                return carte
        return "passe"


def playRandom(plateau, lRoi, cRoi, mainR, mainB, couleur, affichage=False):
    if couleur == "Rouge":
        action = random.choice(coupsJouables(plateau, lRoi, cRoi, mainR))
    else:
        action = random.choice(coupsJouables(plateau, lRoi, cRoi, mainB))
    if affichage: print("action jouée :", action)
    return action



''' FONCTIONS D'OBSERVATION DU JEU '''

def partieTermineeIA(plateau, lRoi, cRoi, mainR, mainB, nbPionR, nbPionB):
    if coupsJouables(plateau, lRoi, cRoi, mainR) == ["passe"] and \
       coupsJouables(plateau, lRoi, cRoi, mainB) == ["passe"]:
        return True
    elif nbPionB == 0 or nbPionR == 0:
        return True
    else:
        return False

def getFirstCardPlayed(plateau, lRoi, cRoi):
    mouvementL = lRoi - len(plateau) // 2
    mouvementC = cRoi - len(plateau[0]) // 2

    if mouvementL != 0:
        nbMoves = abs(mouvementL)
    else:
        nbMoves = abs(mouvementC)

    direction = ""
    if mouvementL < 0:
        direction += "N"
    if mouvementL > 0:
        direction += "S"
    if mouvementC > 0:
        direction += "E"
    if mouvementC < 0:
        direction += "O"

    return str().join((direction, str(nbMoves)))


def observeFirstMove(plateau, l_roi, c_roi, main_r, main_b, couleur):
    global pioche, mainR, mainB, nbPionBlanc, nbPionRouge
    pioche[:] = [carte for carte in pioche if carte not in main_r]
    pioche[:] = [carte for carte in pioche if carte not in main_b]
    mainR = copy.deepcopy(main_r)
    mainB = copy.deepcopy(main_b)

    if couleur == "Blanc": #si on est blanc et au premier coup, on a pas en memoire 
        #les mains pour savoir ce qu'a joué le rouge, il faut donc regarder sur le plateau
        nbPionRouge -= 1
        premiereCarte = getFirstCardPlayed(plateau, l_roi, c_roi)
        defausse.append(premiereCarte)
        pioche.remove(premiereCarte)


def observeLast2Moves(main_r, main_b, couleur):
    global pioche, mainR, mainB, nbPionBlanc, nbPionRouge, defausse

    #on oberve d'abord son ancien coup puis celui que l'adversaire vient de jouer
    ordre = ((mainR, main_r), (mainB, main_b)) 
    if couleur == "Blanc":
        ordre = ((mainB, main_b), (mainR, main_r))
    
    for i, (mainEnMemoire, mainObservee) in enumerate(ordre):
        
        if set(mainObservee) != set(mainEnMemoire): #joueur qui a joué ou pioché
            if len(mainObservee) > len(mainEnMemoire): #joueur a pioché
                if len(pioche) == 0: #on recrée la pioche si elle est vide
                    pioche = copy.deepcopy(defausse)
                    defausse = []
                nvleCarte = mainObservee[-1]
                ordre[i][0].append(nvleCarte)
                pioche.remove(nvleCarte)
            else:                        #joueur a joué
                carteJouee = set(mainEnMemoire) - set(mainObservee) #on cherche la carte de difference
                carteJouee = carteJouee.pop() #la difference des 2 mains est contenue dans un set
                defausse.append(carteJouee)
                ordre[i][0].remove(carteJouee)
                nbPionBlanc -= 1



''' MINIMAX (NEGAMAX) '''

def evalState(plateau, mainR, mainB, couleur):
    if couleur == "Rouge":
        return len(mainR) + score(plateau, "Rouge")*2 - score(plateau, "Blanc")*2
    else:
        return len(mainB) + score(plateau, "Blanc")*2 - score(plateau, "Rouge")*2

def evalNegamax(plateau, lRoi, cRoi, mainR, mainB, pioche, defausse, couleur, nbPionR, nbPionB, profondeur=5):
    if partieTermineeIA(plateau, lRoi, cRoi, mainR, mainB, nbPionR, nbPionB) or profondeur == 0:
        return evalState(plateau, mainR, mainB, couleur)

    dico = {"Rouge":mainR, "Blanc":mainB} #va eviter 2 enormes if
    value = -math.inf

    for action in coupsJouables(plateau, lRoi, cRoi, dico.get(couleur)):
            #on fait une deep copy de l'etat present
            copyPlateau = copy.deepcopy(plateau)
            copyLRoi = lRoi
            copyCRoi = cRoi
            copyMainRouge = copy.deepcopy(mainR)
            copyMainBlanc = copy.deepcopy(mainB)
            copyPioche = copy.deepcopy(pioche)
            copyDefausse = copy.deepcopy(defausse)
            copyCurrentCouleur = couleur
            copyNbPionRouge = nbPionR
            copyNbPionBlanc = nbPionB

            #on obtient la situation enfant
            copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc = \
                effectueAction(copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc, action)
            
            #on applique l'algo sur l'enfant avec prof-1
            value = max(value, -evalNegamax(copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc, profondeur-1))
    return value
    
def playNegamax(plateau, lRoi, cRoi, mainR, mainB, pioche, defausse, couleur, nbPionR, nbPionB):
    value, bestValue, bestAction = -math.inf, -math.inf, -math.inf
    dico = {"Rouge":mainR, "Blanc":mainB}

    for action in coupsJouables(plateau, lRoi, cRoi, dico.get(couleur)):
            #on fait une deep copy de l'etat present
            copyPlateau = copy.deepcopy(plateau)
            copyLRoi = lRoi
            copyCRoi = cRoi
            copyMainRouge = copy.deepcopy(mainR)
            copyMainBlanc = copy.deepcopy(mainB)
            copyPioche = copy.deepcopy(pioche)
            copyDefausse = copy.deepcopy(defausse)
            copyCurrentCouleur = couleur
            copyNbPionRouge = nbPionR
            copyNbPionBlanc = nbPionB

             #on obtient la situation enfant
            copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc = \
                effectueAction(copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc, action)

            value = -evalNegamax(copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc, PROF_MINMAX_ALPHABETA)
            if (value > bestValue):
                bestValue = value
                bestAction = action

    return bestAction


''' ELAGAGE ALPHABETA (FACTORISATION NEGAMAX) '''



def evalAlphaBeta(plateau, lRoi, cRoi, mainR, mainB, pioche, defausse, couleur, nbPionR, nbPionB, profondeur=5, valueAlphaBeta=-math.inf):
    if partieTerminee(plateau, lRoi, cRoi, mainR, mainB, nbPionR, nbPionB) or profondeur == 0:
        return evalState(plateau, mainR, mainB, couleur)

    dico = {"Rouge":mainR, "Blanc":mainB} #va eviter 2 enormes if
    value = -math.inf

    for action in coupsJouables(plateau, lRoi, cRoi, dico.get(couleur)):
            #on fait une deep copy de l'etat present
            copyPlateau = copy.deepcopy(plateau)
            copyLRoi = lRoi
            copyCRoi = cRoi
            copyMainRouge = copy.deepcopy(mainR)
            copyMainBlanc = copy.deepcopy(mainB)
            copyPioche = copy.deepcopy(pioche)
            copyDefausse = copy.deepcopy(defausse)
            copyCurrentCouleur = couleur
            copyNbPionRouge = nbPionR
            copyNbPionBlanc = nbPionB

            #on obtient la situation enfant
            copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc = \
                effectueAction(copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc, action)
            
            #on applique l'algo sur l'enfant avec prof-1
            value = max(value, -evalAlphaBeta(copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc, profondeur-1, value))

            if -value <= valueAlphaBeta: #coupure alphabeta
                return -value 
    return value
    
def playAlphaBeta(plateau, lRoi, cRoi, mainR, mainB, pioche, defausse, couleur, nbPionR, nbPionB):
    value, bestValue, bestAction = -math.inf, -math.inf, -math.inf
    dico = {"Rouge":mainR, "Blanc":mainB}

    for action in coupsJouables(plateau, lRoi, cRoi, dico.get(couleur)):
            #on fait une deep copy de l'etat present
            copyPlateau = copy.deepcopy(plateau)
            copyLRoi = lRoi
            copyCRoi = cRoi
            copyMainRouge = copy.deepcopy(mainR)
            copyMainBlanc = copy.deepcopy(mainB)
            copyPioche = copy.deepcopy(pioche)
            copyDefausse = copy.deepcopy(defausse)
            copyCurrentCouleur = couleur
            copyNbPionRouge = nbPionR
            copyNbPionBlanc = nbPionB

             #on obtient la situation enfant
            copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc = \
                effectueAction(copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc, action)

            value = -evalAlphaBeta(copyPlateau, copyLRoi, copyCRoi, copyMainRouge, copyMainBlanc, copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc, PROF_MINMAX_ALPHABETA)
            if (value > bestValue):
                bestValue = value
                bestAction = action

    return bestAction



 
'''  MONTE CARLE TREE SEARCH  '''


class State:
    def __init__(self, plateau, lRoi, cRoi, mainR, mainB, pioche, defausse, couleur, nbPionR, nbPionB): 
        self.plateau, self.lRoi, self.cRoi, self.mainR, self.mainB, self.pioche, self.defausse, self.couleur, self.nbPionR, self.nbPionB = \
            plateau, lRoi, cRoi, mainR, mainB, pioche, defausse, couleur, nbPionR, nbPionB

    def isTerminal(self):
        return partieTerminee(self.plateau, self.lRoi, self.cRoi, self.mainR, self.mainB, self.nbPionR, self.nbPionB)

    def getDeepCopy(self):
        #on fait une deep copy de l'etat present
        copyPlateau = copy.deepcopy(self.plateau)
        copyLRoi = self.lRoi
        copyCRoi = self.cRoi
        copyMainRouge = copy.deepcopy(self.mainR)
        copyMainBlanc = copy.deepcopy(self.mainB)
        copyPioche = copy.deepcopy(self.pioche)
        copyDefausse = copy.deepcopy(self.defausse)
        copyCurrentCouleur = self.couleur
        copyNbPionRouge = self.nbPionR
        copyNbPionBlanc = self.nbPionB
        return State(copyPlateau, copyLRoi, copyCRoi, copyMainRouge,  copyMainBlanc,  copyPioche, copyDefausse, copyCurrentCouleur, copyNbPionRouge, copyNbPionBlanc)

    def eval(self, couleur):
        if getWinner(self.plateau) == couleur:
            return "win"
        else:
            return "lose"

    def addMove(self, move):
        return State(*effectueAction(self.plateau, self.lRoi, self.cRoi, self.mainR, self.mainB, self.pioche, self.defausse, self.couleur, self.nbPionR, self.nbPionB, move, False)) #le False : on ne remelange pas la pioche d'une selection à l'autre

    def simulationRandom(self):
        move = playRandom(self.plateau, self.lRoi, self.cRoi, self.mainR, self.mainB, self.couleur)
        return State(*effectueAction(self.plateau, self.lRoi, self.cRoi, self.mainR, self.mainB, self.pioche, self.defausse, self.couleur, self.nbPionR, self.nbPionB, move))

    

class Node:
    def __init__(self, move, parent): # move est le lien entre parent et lui-meme
        self.move, self.parent, self.children = move, parent, []
        self.wins, self.visits  = 0, 0

    def uct(self):
        ratioChild, ratioParentChild = int(), int()

        if self.visits == 0: 
            return math.inf
        else : 
            ratioChild = self.wins / self.visits
            ratioParentChild = math.sqrt(math.log(self.parent.visits) / self.visits)

        constant = 5

        return ratioChild + constant * ratioParentChild
   
    def selectNextNode(self):
        uctMax, childMax = 0, Node(0,0)
        for child in self.children:
            if child.uct() >= uctMax:
                uctMax = child.uct()
                childMax = child
        return childMax

    def expandNode(self, state):
        dicoColor = {"Blanc":state.mainB, "Rouge":state.mainR}
        for move in coupsJouables(state.plateau, state.lRoi, state.cRoi, dicoColor[state.couleur]):
            newChild = Node(move, self) # new child node
            self.children.append(newChild)

    def update(self, result):
        self.visits += 1
        if result == "win":
            self.wins += 1

    def isLeaf(self):
        return len(self.children) == 0

    def hasParent(self):
        return self.parent is not None

    

    
def mcts(state):
    root_node = Node(None, None)

    timeout_start = time.time()
    while time.time() < timeout_start + SEARCH_TIME_MCTS: #secondes de reflexion
        node, simul = root_node, state.getDeepCopy()
        color = state.couleur 

        # SELECTION NOEUD JUSQU'A UNE FEUILLE
        while not node.isLeaf(): 
            node = node.selectNextNode()
            simul = simul.addMove(node.move)

        # EXPANSION
        node.expandNode(simul)          
        node = node.selectNextNode()
        #simul = simul.addMove(node.move)

        # SIMULATION ALEATOIRE JUSQUE FIN DE PARTIE
        while not simul.isTerminal():    
            simul = simul.simulationRandom()
        result = simul.eval(color)

        node.update(result)

        # RETRO PROPAGATION
        while node.hasParent():   
            node = node.parent
            node.update(result)
            

    nodeMax = -1
    nodeToReturn = None
    for node in root_node.children:
        if node.visits != 0:
            if node.wins / node.visits > nodeMax:
                nodeMax = node.wins / node.visits
                nodeToReturn = node
    
    return nodeToReturn.move