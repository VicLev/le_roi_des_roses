import random
import time
import ia

''' INIT '''

def initPioche():
    pioche = []
    for i in range(1,4):
        for j in ["N","NE","E","SE","S","SO","O","NO"]:
            pioche.append(j+str(i))
    return pioche

def init_jeu(nb_lignes, nb_colonnes):
    #plateau
    plateau = []
    for _ in range(nb_lignes):
        line = []
        for _ in range(nb_colonnes):
            line.append(".")
        plateau.append(line)
    
    #placement du roi au milieu
    lRoi, cRoi = nb_lignes//2, nb_colonnes//2

    #creation, melange et repartition de la pioche dans les mains
    pioche = initPioche()
    random.shuffle(pioche)

    mainR = [pioche[i] for i in range(5)]
    pioche = pioche[5:]
    mainB = [pioche[i] for i in range(5)]
    pioche = pioche[5:]

    #defausse vide à l'init
    defausse = []
    
    return plateau, lRoi, cRoi, mainR, mainB, pioche, defausse


''' AFFICHAGE '''

def afficher_main(couleur, main):
    message = couleur + " : "
    for carte in main:
        message += carte + " "
    print(message[:-1])


def afficher_jeu(plateau, l_roi, c_roi, main_r, main_b):
    affichagePlateau = "-" * len(plateau)*5 + "-\n"  #premiere ligne

    for i in range(len(plateau)):
        affichagePlateau += "| " #premiere colonne
        for j in range(len(plateau[0])):
            #presence couronne
            if (i, j) == (l_roi, c_roi):
                affichagePlateau += "X"
            else:
                affichagePlateau += " "
            
            #presence pion
            if plateau[i][j] == ".":
                affichagePlateau += " " + " | "
            else:
                affichagePlateau += plateau[i][j] + " | "

            if j == len(plateau[0])-1: #derniere colonne
                affichagePlateau = affichagePlateau[:-1]

        #affichage lignes + retours chariot
        affichagePlateau += "\n" + "-" * len(plateau)*5 + "-\n"

    afficher_main("Rouge", main_r)
    afficher_main("Blanc", main_b)
    print(affichagePlateau[:-1])


def afficherFinDePartie(plateau):
    scoreR, scoreB = score(plateau, "Rouge"), score(plateau, "Blanc")
    message = "Rouge " + str(scoreR) + "\n"
    message += "Blanc " + str(scoreB) + "\n"
    if scoreB > scoreR:
        message += "Blanc a gagné la partie"
    elif scoreB < scoreR:
        message += "Rouge a gagné la partie"
    else:
        message += "Egalité"
    print(message)


''' VALID MOVES '''

def mouvement_possible(plateau, l_roi, c_roi, carte):
    lFinal, cFinal = l_roi, c_roi
    nbDeplacement = int(carte[-1:])
    direction = carte[:-1]

    #on bouge la Couronne en fonction de la carte influence
    #direction * numero de la carte
    if ('N' in direction): lFinal -= nbDeplacement
    if ('S' in direction): lFinal += nbDeplacement
    if ('O' in direction): cFinal -= nbDeplacement
    if ('E' in direction): cFinal += nbDeplacement

    #on sort pas du plateau
    if (lFinal<0 or lFinal>8 or cFinal<0 or cFinal>8):
        return False
    elif (plateau[lFinal][cFinal] != "."):
        return False 
    return True


def main_jouable(plateau, l_roi, c_roi, main):
    carteJouables = []
    for carte in main:
        if mouvement_possible(plateau, l_roi, c_roi, carte):
            carteJouables.append(carte)
    return carteJouables


def coupsJouables(plateau, lRoi, cRoi, main):
    coupsJouables = []
    if len(main) < 5:
        coupsJouables.append("pioche")
    for carteJouable in main_jouable(plateau, lRoi, cRoi, main):
        coupsJouables.append(carteJouable)
    
    if not coupsJouables:
        coupsJouables.append("passe")
    return coupsJouables


''' JOUER COUPS '''

def demande_action(couleur, plateau, l_roi, c_roi, main):
    print("Vous etes le joueur", couleur, "que souhaitez-vous faire ? ")
    
    reponsesPossibles = coupsJouables(plateau, l_roi, c_roi, main)
    reponse = input()

    while reponse not in reponsesPossibles:
        reponse = input("Action impossible, que souhaitez-vous faire ? ")

    return reponse


def playHuman(couleur, plateau, lRoi, cRoi, mainR, mainB):
    if couleur == "Rouge":
        return demande_action(couleur, plateau, lRoi, cRoi, mainR)
    else:
        return demande_action(couleur, plateau, lRoi, cRoi, mainB)


def bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, carte, couleur):
    lFinal, cFinal = l_roi, c_roi
    nbDeplacement = int(carte[-1:])
    direction = carte[:-1]

    #on bouge la Couronne en fonction de la carte influence
    #direction * numero de la carte
    if ('N' in direction): lFinal -= nbDeplacement
    if ('S' in direction): lFinal += nbDeplacement
    if ('O' in direction): cFinal -= nbDeplacement
    if ('E' in direction): cFinal += nbDeplacement

    if couleur == "Rouge":
        plateau[lFinal][cFinal] = "R"
        #print("mainr:", main_r, "// carte:", carte)
        #print("mainb:", main_b)
        main_r.remove(carte)
    else:
        plateau[lFinal][cFinal] = "B"
        #print("mainb:", main_b, "// carte:", carte)
        #print("mainr:", main_r)
        main_b.remove(carte)

    defausse.append(carte)

    return plateau, lFinal, cFinal, main_r, main_b, defausse


def piocher(mainR, mainB, pioche, defausse, couleur, shuffling=True):
    if not pioche:
        if shuffling: #ne doit pas remelanger la pioche lors de la phase selection du MCTS
            random.shuffle(defausse)
        pioche = defausse
        defausse = []

    if couleur == "Rouge":
        mainR.append(pioche[0])
    else:
        mainB.append(pioche[0])
    pioche = pioche[1:]
    
    return mainR, mainB, pioche, defausse


def effectueAction(plateau, lRoi, cRoi, mainR, mainB, pioche, defausse, couleur, nbPR, nbPB, action, shufflingDraw=True):
    if action == "passe":
        pass #c'est pourtant evident

    elif action == "pioche":
        mainR, mainB, pioche, defausse = piocher(mainR, mainB, pioche, defausse, couleur, shufflingDraw)

    else:
        plateau, lRoi, cRoi, mainR, mainB, defausse = \
        bouge_le_roi(plateau, lRoi, cRoi, mainR, mainB, defausse, action, couleur)
        #on reduit le nb de pion
        if couleur == "Rouge": nbPR -= 1
        else: nbPB -= 1

    #on change de joueur
    if couleur == "Rouge": couleur = "Blanc"
    else: couleur = "Rouge"
    
    return plateau, lRoi, cRoi, mainR, mainB, pioche, defausse, couleur, nbPR, nbPB


''' CALCUL SCORE  '''

def territoire(plateau, ligne, colonne, couleur):
    couleur = couleur[0] #on prend la première lettre pour verifier avec le plateau

    if plateau[ligne][colonne] != couleur:
        return []
    else:
        #3 listes : territoire pour le return,
        #opened pour la liste des cases interessante, dont on va devoir explorer les voisins ensuite
        #closed pour la liste des cases deja vues dont on ne veut pas voir les voisins car deja visitée ou pas de la bonne couleur
        territoire = [(ligne, colonne)]
        opened = [(ligne, colonne)]
        closed = []
        directions = [(1,0),(0,1),(-1,0),(0,-1)]

        while len(opened) != 0:
            for case in opened: #on visite toutes les cases prometteuses
                for direction in directions: #on regarde a partir de la case, ses voisins
                    caseExploree = tuple([a+b for a,b in zip(case, direction)]) #un des voisins
                    if caseExploree not in closed and caseExploree not in opened: #si on ne l'a pas deja vu
                        #si ca ne sort pas du plateau
                        if caseExploree[0] > 0 and caseExploree[0] < len(plateau) and caseExploree[1] > 0 and caseExploree[1] < len(plateau):
                            if plateau[caseExploree[0]][caseExploree[1]] == couleur:
                                territoire.append(caseExploree)
                                opened.append(caseExploree)
                            else:
                                closed.append(caseExploree)                            
                opened.remove(case)
                closed.append(case)
        return territoire


def score(plateau, couleur):
    closed = []
    score = 0
    for i in range(len(plateau)):
        for j in range(len(plateau[0])):
            if plateau[i][j] == couleur[0] and (i,j) not in closed:
                score += len(territoire(plateau, i, j, couleur))**2
                closed.extend(territoire(plateau, i, j, couleur))
    return score


''' ENDGAME '''

def partieTerminee(plateau, lRoi, cRoi, mainR, mainB, nbPionR, nbPionB):
    if coupsJouables(plateau, lRoi, cRoi, mainR) == ["passe"] and \
       coupsJouables(plateau, lRoi, cRoi, mainB) == ["passe"]:
        return True
    elif nbPionB == 0 or nbPionR == 0:
        return True
    else:
        return False


def getWinner(plateau):
    if score(plateau, "Rouge") > score(plateau, "Blanc"):
        return "Rouge"
    elif score(plateau, "Rouge") < score(plateau, "Blanc"):
        return "Blanc"
    else:
        return "Egalite"


''' BOUCLE DE JEU '''

def main():
    plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse = init_jeu(9,9)
    nbPionRouge, nbPionBlanc = 26, 26
    currentCouleur = "Rouge"

    while not partieTerminee(plateau, lRoi, cRoi, mainRouge, mainBlanc, nbPionRouge, nbPionBlanc):
        afficher_jeu(plateau, lRoi, cRoi, mainRouge, mainBlanc) #on affiche le plateau
        #on choisit une action
        #action = playHuman(currentCouleur, plateau, lRoi, cRoi, mainRouge, mainBlanc)
        #action = ia.playTataSuzanne(plateau, lRoi, cRoi, mainRouge, mainBlanc, currentCouleur)
        #action = ia.playRandom(plateau, lRoi, cRoi, mainRouge, mainBlanc, currentCouleur)
        #action = ia.playAlphaBeta(plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse, currentCouleur, nbPionRouge, nbPionBlanc)
        #action = ia.playNegamax(plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse, currentCouleur, nbPionRouge, nbPionBlanc)

        if currentCouleur == "Rouge" : 
            action = ia.playRandom(plateau, lRoi, cRoi, mainRouge, mainBlanc, currentCouleur)
        else:
            action = ia.play(plateau, lRoi, cRoi, mainRouge, mainBlanc, currentCouleur)

        print("Action jouee :", action, "/", currentCouleur, "\n")
        

        #on realise l'action, le changement de joueur s'effectue également
        plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse, currentCouleur, nbPionRouge, nbPionBlanc = \
            effectueAction(plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse, currentCouleur, nbPionRouge, nbPionBlanc, action)



    #on sort de la boucle, partie terminée
    afficherFinDePartie(plateau)


def nGames(n):
    start_time = time.time()

    nbWins = [0, 0, 0] #[rouge, blanc, egalite]
    for i in range(n):
        print('game', i)
        plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse = init_jeu(9,9)
        nbPionRouge, nbPionBlanc = 26, 26
        currentCouleur = "Rouge"

        ia.reset() #on reset les variables globales "d'observation" de l'ia

        while not partieTerminee(plateau, lRoi, cRoi, mainRouge, mainBlanc, nbPionRouge, nbPionBlanc):
            #action = ia.playTataSuzanne(plateau, lRoi, cRoi, mainRouge, mainBlanc, currentCouleur)
            #action = ia.playRandom(plateau, lRoi, cRoi, mainRouge, mainBlanc, currentCouleur)
            #action = ia.playNegamax(plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse, currentCouleur, nbPionRouge, nbPionBlanc)
            #action = ia.playAlphaBeta(plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse, currentCouleur, nbPionRouge, nbPionBlanc)
            #action = ia.play(plateau, lRoi, cRoi, mainRouge, mainBlanc, currentCouleur)

            if currentCouleur == "Rouge" : 
                action = ia.playRandom(plateau, lRoi, cRoi, mainRouge, mainBlanc, currentCouleur)
            else:
                action = ia.play(plateau, lRoi, cRoi, mainRouge, mainBlanc, currentCouleur)


            plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse, currentCouleur, nbPionRouge, nbPionBlanc = \
                effectueAction(plateau, lRoi, cRoi, mainRouge, mainBlanc, pioche, defausse, currentCouleur, nbPionRouge, nbPionBlanc, action)

        if getWinner(plateau) == "Rouge": nbWins[0]+=1 
        elif getWinner(plateau) == "Blanc": nbWins[1]+=1 
        else: nbWins[2]+=1 
        #print("partie", i, "finie")

    print("Rouge:", nbWins[0], ", Blanc:", nbWins[1], "Egalité:", nbWins[2])
    print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == "__main__":
    print(ia.name())
    print("random vs mcts")
    main()
    #nGames(100)
    
